package dev.sebastianoverdolini.amazon.elasticsearch

import dev.sebastianoverdolini.amazon.domain.Product
import dev.sebastianoverdolini.amazon.domain.ProductRepository
import kotlinx.serialization.json.*
import java.net.Authenticator
import java.net.PasswordAuthentication
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class ElasticSearchProductRepository(
    private val host: String,
    private val port: Int,
    private val index: String) : ProductRepository()
{
    private val client = HttpClient.newBuilder()
        .build()

    override fun add(product: Product)
    {
        client.send(
            HttpRequest.newBuilder()
                .uri(URI.create("http://$host:$port/$index/_doc/${product.id}"))
                .header("Content-Type", "application/json")
                .PUT(
                    HttpRequest.BodyPublishers.ofString(
                        """
                        {
                            "description": "${product.description}",
                            "price": ${product.price}
                        }
                        """.trimIndent()))
                .build(),
            HttpResponse.BodyHandlers.ofString())
    }

    override fun products(): Set<Product>
    {
        val req = HttpRequest
            .newBuilder(URI.create("http://$host:$port/$index/_search"))
            .build()
        val resp = client.send(req, HttpResponse.BodyHandlers.ofString())
        return Json.parseToJsonElement(resp.body())
            .jsonObject["hits"]!!
            .jsonObject["hits"]!!
            .jsonArray
            .map { it.jsonObject }
            .map { parseProductFromElasticSearchJson(it) }
            .toSet()
    }

    override fun product(id: String): Product?
    {
        val req = HttpRequest
            .newBuilder(URI.create("http://$host:$port/$index/_doc/$id"))
            .build()
        val resp = client.send(req, HttpResponse.BodyHandlers.ofString())
        val json = Json.parseToJsonElement(resp.body()).jsonObject
        return if(json["found"]!!.jsonPrimitive.content.toBoolean())
            parseProductFromElasticSearchJson(json) else null
    }

    private fun parseProductFromElasticSearchJson(hit: JsonObject): Product
    {
        return Product(
            hit["_id"]!!.jsonPrimitive.content,
            hit["_source"]!!.jsonObject["description"]!!.jsonPrimitive.content,
            hit["_source"]!!.jsonObject["price"]!!.jsonPrimitive.content.toDouble())
    }
}