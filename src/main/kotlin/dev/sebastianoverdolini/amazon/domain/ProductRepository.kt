package dev.sebastianoverdolini.amazon.domain

abstract class ProductRepository
{
    abstract fun add(product: Product)
    abstract fun products(): Set<Product>
    open fun product(id: String): Product?
    {
        return products().firstOrNull { it.id == id }
    }
}