package dev.sebastianoverdolini.amazon.domain

import kotlinx.serialization.Serializable

@Serializable
data class Product(
    val id: String,
    val description: String,
    val price: Double)