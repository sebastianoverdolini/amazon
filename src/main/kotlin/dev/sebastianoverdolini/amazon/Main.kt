package dev.sebastianoverdolini.amazon

import dev.sebastianoverdolini.amazon.elasticsearch.ElasticSearchProductRepository
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.coroutines.runBlocking

fun main()
{
    embeddedServer(Netty, port = 8080) {
        main()
    }.start(wait = true)
}

fun Application.main()
{
    val productRepository = ElasticSearchProductRepository(
        environment.config.propertyOrNull("es.host")?.getString() ?: "localhost",
        environment.config.propertyOrNull("es.port")?.getString()?.toInt() ?: 9200,
        "products")
    val randomId = { randomFiveCharLengthString() }
    routing {
        route("/products") {
            post {
                addProduct(
                    productRepository,
                    randomId,
                    { runBlocking { call.respond(HttpStatusCode.Created) } },
                    call.receive()
                )
            }
            get {
                call.respond(productRepository.products().sortedBy { it.description })
            }
            get("{id?}") {
                call.respond(
                    productRepository.product(call.parameters["id"]!!)
                        ?: HttpStatusCode.NotFound
                )
            }
        }
    }
    install(ContentNegotiation) {
        json()
    }
}

fun randomFiveCharLengthString(): String
{
    return (1..5)
        .map { (('A'..'Z') + ('a'..'z') + ('0'..'9')).random() }
        .joinToString("")
}