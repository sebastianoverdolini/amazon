package dev.sebastianoverdolini.amazon

import dev.sebastianoverdolini.amazon.domain.Product
import dev.sebastianoverdolini.amazon.domain.ProductRepository
import kotlinx.serialization.Serializable

fun addProduct(
    productRepository: ProductRepository,
    randomId: () -> String,
    success: () -> Unit,
    req: AddProductRequest)
{
    productRepository.add(Product(randomId(), req.description, req.price))
    success()
}

@Serializable
data class AddProductRequest(
    val description: String,
    val price: Double)