package dev.sebastianoverdolini.amazon

import dev.sebastianoverdolini.amazon.domain.Product
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class UseCasesTest : StringSpec({
    "add product" {
        var success = false
        val productRepository = InMemoryProductRepository(mutableSetOf())
        addProduct(productRepository, { "v86Ot" }, { success = true }, AddProductRequest("Milk", 1.50))
        productRepository.products shouldBe setOf(Product("v86Ot", "Milk", 1.50))
        success shouldBe true
    }
})
