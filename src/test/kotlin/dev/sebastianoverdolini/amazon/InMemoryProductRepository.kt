package dev.sebastianoverdolini.amazon

import dev.sebastianoverdolini.amazon.domain.Product
import dev.sebastianoverdolini.amazon.domain.ProductRepository

class InMemoryProductRepository(val products: MutableSet<Product>) : ProductRepository()
{
    override fun add(product: Product)
    {
        products.add(product)
    }

    override fun products(): Set<Product>
    {
        return products
    }
}