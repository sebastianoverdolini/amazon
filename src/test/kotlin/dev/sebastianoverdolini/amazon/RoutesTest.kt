package dev.sebastianoverdolini.amazon

import dev.sebastianoverdolini.amazon.elasticsearch.*
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.server.config.*
import io.ktor.server.testing.*
import kotlinx.serialization.json.Json
import org.testcontainers.elasticsearch.ElasticsearchContainer

const val INDEX = "products"

class RoutesTest : StringSpec({
    val elasticsearch = installElasticsearchContainer()

    beforeEach { elasticsearch.create(INDEX) }
    afterEach { elasticsearch.delete(INDEX) }

    "add product" {
        testAmazonApplication(elasticsearch) {
            val response = client.post("/products") {
                contentType(ContentType.Application.Json)
                setBody("""
                    {
                        "description": "TV",
                        "price": 300
                    }
                """.trimIndent())
            }
            elasticsearch.refresh(INDEX)

            response.status shouldBe HttpStatusCode.Created
            elasticsearch.count(INDEX) shouldBe 1
            Json.parseToJsonElement(elasticsearch.searchFirstSource(INDEX)) shouldBe
                    Json.parseToJsonElement("""
                        {
                            "description": "TV",
                            "price": 300.0
                        }
                    """.trimIndent())
        }
    }

    "products returns alphabetical sorted" {
        testAmazonApplication(elasticsearch) {
            elasticsearch.load(
                INDEX,
                "v86Ot",
                """
                    {
                        "description": "Milk",
                        "price": 1.5
                    }
                """.trimIndent())
            elasticsearch.load(
                INDEX,
                "hVxD1",
                """
                    {
                        "description": "Bread",
                        "price": 3
                    }
                """.trimIndent())
            val response = client.get("/products")

            response.status shouldBe HttpStatusCode.OK
            Json.parseToJsonElement(response.bodyAsText()) shouldBe
                    Json.parseToJsonElement("""
                        [
                            {
                                "id": "hVxD1",
                                "description": "Bread",
                                "price": 3.0
                            },
                            {
                                "id": "v86Ot",
                                "description": "Milk",
                                "price": 1.5
                            }
                        ]
                    """.trimIndent())
        }
    }

    "product existent" {
        testAmazonApplication(elasticsearch) {
            elasticsearch.load(
                INDEX,
                "v86Ot",
                """
                    {
                        "description": "Milk",
                        "price": 1.5
                    }
                """.trimIndent())
            val response = client.get("/products/v86Ot")

            response.status shouldBe HttpStatusCode.OK
            Json.parseToJsonElement(response.bodyAsText()) shouldBe
                    Json.parseToJsonElement("""
                        {
                            "id": "v86Ot",
                            "description": "Milk",
                            "price": 1.5
                        }
                    """.trimIndent())
        }
    }

    "product non existent" {
        testAmazonApplication(elasticsearch) {
            val response = client.get("/products/h67Ik")

            response.status shouldBe HttpStatusCode.NotFound
        }
    }
})

fun testAmazonApplication(
    elasticsearch: ElasticsearchContainer,
    block: suspend ApplicationTestBuilder.() -> Unit): Unit =
    testApplication {
        application {
            main()
        }
        environment {
            val c = MapApplicationConfig()
            c.put("es.host", elasticsearch.host)
            c.put("es.port", elasticsearch.getMappedPort(9200).toString())
            config = c
        }
        block()
    }