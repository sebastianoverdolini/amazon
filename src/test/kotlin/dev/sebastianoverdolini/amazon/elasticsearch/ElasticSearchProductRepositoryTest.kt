package dev.sebastianoverdolini.amazon.elasticsearch

import dev.sebastianoverdolini.amazon.domain.Product
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

const val INDEX = "products"

class ElasticSearchProductRepositoryTest : StringSpec({
    val elasticsearch = installElasticsearchContainer()
    beforeEach { elasticsearch.create(INDEX) }
    afterEach { elasticsearch.delete(INDEX) }

    val productRepository = ElasticSearchProductRepository(
        elasticsearch.host, elasticsearch.getMappedPort(9200), INDEX)

    "add product" {
        productRepository.add(Product("v86Ot", "Milk", 1.50))
        elasticsearch.refresh(INDEX)
        productRepository.products() shouldBe
                setOf(Product("v86Ot", "Milk", 1.50))
    }

    "all products" {
        productRepository.add(Product("v86Ot", "Milk", 1.50))
        productRepository.add(Product("hVxD1", "Bread", 3.0))
        elasticsearch.refresh(INDEX)
        productRepository.products() shouldBe setOf(
            Product("v86Ot", "Milk", 1.50),
            Product("hVxD1", "Bread", 3.0)
        )
    }

    "product by existent id" {
        productRepository.add(Product("v86Ot", "Milk", 1.50))
        productRepository.add(Product("hVxD1", "Bread", 3.0))
        elasticsearch.refresh(INDEX)
        productRepository.product("v86Ot") shouldBe
                Product("v86Ot", "Milk", 1.50)
    }

    "product by non existent id" {
        productRepository.add(Product("v86Ot", "Milk", 1.50))
        productRepository.add(Product("hVxD1", "Bread", 3.0))
        elasticsearch.refresh(INDEX)
        productRepository.product("j2f5h") shouldBe null
    }
})