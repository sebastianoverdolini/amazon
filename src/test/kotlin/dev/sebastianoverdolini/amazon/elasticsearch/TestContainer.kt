package dev.sebastianoverdolini.amazon.elasticsearch

import io.kotest.core.extensions.install
import io.kotest.core.spec.Spec
import io.kotest.extensions.testcontainers.TestContainerExtension
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import org.testcontainers.elasticsearch.ElasticsearchContainer
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpRequest.BodyPublishers
import java.net.http.HttpResponse

fun Spec.installElasticsearchContainer(): ElasticsearchContainer =
    install(
        TestContainerExtension(
            ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch:8.3.3")))
    {
        withEnv("xpack.security.enabled", "false")
    }

private val ElasticsearchContainer.url: String
    get() = "http://$host:${getMappedPort(9200)}"

private fun ElasticsearchContainer.indexUrl(index: String): String = "${url}/$index"

fun ElasticsearchContainer.create(index: String)
{
    HttpClient.newHttpClient().send(
        HttpRequest.newBuilder()
            .uri(URI.create(indexUrl(index)))
            .PUT(BodyPublishers.noBody())
            .build(),
        HttpResponse.BodyHandlers.ofString())
}

fun ElasticsearchContainer.load(index: String, id: String, json: String)
{
    HttpClient.newHttpClient().send(
        HttpRequest.newBuilder()
            .uri(URI.create("${indexUrl(index)}/_doc/$id?refresh"))
            .header("Content-Type", "application/json")
            .PUT(BodyPublishers.ofString(json))
            .build(),
        HttpResponse.BodyHandlers.ofString())
}

fun ElasticsearchContainer.count(index: String): Int
{
    val req = HttpRequest
        .newBuilder(URI.create("${indexUrl(index)}/_search"))
        .build()
    val resp = HttpClient.newHttpClient().send(req, HttpResponse.BodyHandlers.ofString())
    return Json.parseToJsonElement(resp.body())
        .jsonObject["hits"]!!
        .jsonObject["total"]!!
        .jsonObject["value"]!!
        .jsonPrimitive
        .toString()
        .toInt()
}

fun ElasticsearchContainer.searchFirstSource(index: String): String
{
    val req = HttpRequest
        .newBuilder(URI.create("${indexUrl(index)}/_search"))
        .build()
    val resp = HttpClient.newHttpClient().send(req, HttpResponse.BodyHandlers.ofString())
    return Json.parseToJsonElement(resp.body())
        .jsonObject["hits"]!!
        .jsonObject["hits"]!!
        .jsonArray
        .first()
        .jsonObject["_source"]!!
        .jsonObject
        .toString()
}

fun ElasticsearchContainer.delete(index: String)
{
    HttpClient.newHttpClient().send(
        HttpRequest.newBuilder()
            .uri(URI.create(indexUrl(index)))
            .DELETE()
            .build(),
        HttpResponse.BodyHandlers.ofString())
}

fun ElasticsearchContainer.refresh(index: String)
{
    HttpClient.newHttpClient().send(
        HttpRequest.newBuilder()
            .uri(URI.create("${indexUrl(index)}/_refresh"))
            .POST(BodyPublishers.noBody())
            .build(),
        HttpResponse.BodyHandlers.ofString())
}